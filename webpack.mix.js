const mix = require('laravel-mix');

require('laravel-mix-polyfill');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .vue()
   .sass('resources/assets/sass/app.scss', 'public/css')
   .polyfill({
       enable: true,
       useBuiltIns: "usage",
       targets: "firefox 50, IE 11"
   })
   .version();
